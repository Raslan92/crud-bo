using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoApi.Models
{
    public class Department
    {
        [Key]
        [Column]
        public int DeptCode{get;set;}

        [Column(TypeName="varchar(100)")]
        public string DepartmentName{get;set;}

         public ICollection<Employee> Employees {get;set;}
    }
}