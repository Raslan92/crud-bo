using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DepartmentController:ControllerBase
    {
      private readonly TodoApiDbContext _context;
      public DepartmentController(TodoApiDbContext context)
      {
        _context=context;   
      }
      [HttpGet]
      public async Task<ActionResult<IEnumerable<Department>>> GetDeaprtments(){
        return await  _context.Mohamed_Departments.ToListAsync();
      }
      [HttpGet("{id}")]
        public async Task<ActionResult<Department>> GetDepartment(int id)
        {
          var department = await _context.Mohamed_Departments.FindAsync(id);
          if(department == null){
              return NotFound();
          }

          return department;
        }
      //post
      [HttpPost]
      public async Task<ActionResult<Department>> PostDepartment(Department department)
      {
        _context.Mohamed_Departments.Add(department);
        await _context.SaveChangesAsync();

          //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
          return CreatedAtAction(nameof(GetDepartment), new { id = department.DeptCode }, department);
      }

       //put : api/employee
      [HttpPut("{id}")]
      public async Task<IActionResult> PutDepartment(int id, Department department)
      {
        var mdepartment = await _context.Mohamed_Departments.FindAsync(id);
            if (mdepartment==null)
            {
                return BadRequest();
            }

            _context.Entry(department).State = EntityState.Modified;
             _context.Mohamed_Departments.Update(department);
            try
            {
               
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DepartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
         //delete 
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDepartment(int id)
        {
            var department = await _context.Mohamed_Departments.FindAsync(id);
            if (department == null)
            {
                return NotFound();
            }

            _context.Mohamed_Departments.Remove(department);
           
            
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool DepartmentExists(int id)
        {
            return _context.Mohamed_Departments.Any(e => e.DeptCode == id);
        }

    }
}